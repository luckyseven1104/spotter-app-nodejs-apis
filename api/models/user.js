/**
 * Module dependencies.
 */

import timestamps from 'mongoose-timestamp';
import update from 'mongoose-model-update';
var crypto = require ('crypto');
import mongoose, { Schema } from 'mongoose';
import P from 'bluebird';

/**
 * User Schema
 */

var User = new Schema ({
    fb_id: {
        type: String,
        unique: true,
        required: 'Facebook ID is required. '
    },
    userID: {type: String},
    first_name: {type: String, required: true},
    last_name: {type: String, required: true},
    chat_id: {type: String, required: true},
    avatar: {type: String, default: ''},
    lifeStyle: {
        WeightTraining: {type: Boolean, default: false},
        OutdoorSports: {type: Boolean, default: false},
        Yoga: {type: Boolean, default: false},
        TeamSports: {type: Boolean, default: false},
        Cycling: {type: Boolean, default: false},
        Hiking: {type: Boolean, default: false},
        MMABoxing: {type: Boolean, default: false},
        Running: {type: Boolean, default: false}
    },
    height: {type: Number},
    education: {type: String},
    career: {type: String},
    style: {type: String},
    ethnicity: {type: String},
    age: {type: Number},
    gender: {type: Number},
    pos: {
        type: [Number],//latitude, longitude
        index: '2d'
    },
    radar_state: {type: Boolean, default: true},
    about: {type: String},
    state: {type: Boolean, default: false}//true:login, false:logout
});

/**
 * Methods
 */

User.methods = {

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */

    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */

    updatePassword: async function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashed_password = this.encryptPassword(password);
        console.log(this);
        return (await this.saveAsync())[0];
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */

    makeSalt: function() {
        return Math.round((new Date().valueOf() * Math.random())) + ''
    },

    addAvatar: function(avatar){
        this.image = avatar

    },

    getAvatar: function(){
        return this.image
    }
};

module.exports = mongoose.model('User', User);
P.promisifyAll(module.exports);
P.promisifyAll(module.exports.prototype);
