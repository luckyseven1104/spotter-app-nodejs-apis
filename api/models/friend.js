/**
 * Created by dev on 16-1-12.
 */

import mongoose, { Schema } from 'mongoose';
import P from 'bluebird';
import timestamps from 'mongoose-timestamp';
import update from 'mongoose-model-update';

/**
 * Friend Schema
 */

var FriendSchema = new Schema ({
    userID: {
        type: Schema.ObjectId,
        ref:'User'
    },
    like: [{
        type: Schema.ObjectId,
        ref:'User'
    }],
    dislike: [{
        type: Schema.ObjectId,
        ref:'User'
    }]
});

module.exports = mongoose.model('Friend', FriendSchema);
P.promisifyAll(module.exports);
P.promisifyAll(module.exports.prototype);
