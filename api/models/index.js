import database from '../../config/database';

module.exports = {
    User: require('./user'),
    Friend: require('./friend'),
    SuccessRate: require('./successrate')
};

