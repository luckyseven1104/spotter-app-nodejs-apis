let jwt    = require('jsonwebtoken');
let env = process.env.NODE_ENV || 'development';
let config = require('../../config/config')[env];

import { User } from '../models';
import { Friend } from '../models';
var fs = require('fs');


async function spotterLogin(req, res, next){

    var fb_id = req.body.fb_id;

    if(!fb_id || fb_id == "") {
        return res.send({
            status: 400,
            reason: "Facebook id is not valid."
        });
    }

    let user = await User.findOneAsync({fb_id: fb_id});

    if(!user){
        signup(req, res, next);
    }

    let token = jwt.sign(user, config.secret, {
        expiresInMinutes: 1440 // expires in 24 hours
    });
    res.success({id: user._id, api_token: token});
}

async function signup(req, res, next){
    var user = new User(req.body);
    user.userID = user._id;
    user.state = true;

    user.save(function (err){
        if(err){
            var errMsg = "";
            for( var key in err.errors){
                if(err.errors.hasOwnProperty(key)){
                    errMsg = errMsg + err.errors[key].message;
                }
            }

            return res.error("register_error", 400, errMsg);
        }
        User.findOne({ fb_id: user.fb_id }, function (err, doc) {
            if (err) { return done(err) }

            var token = jwt.sign(doc, config.secret, {
                expiresInMinutes: 1440 // expires in 24 hours
            });

            var response = {};
            response.id = doc._id;
            response.api_token = token;
            return res.success(response);
        })
    });
}

async function spotterDelete(req, res, next){
    await req.user.removeAsync();
    return res.success();
}

async function spotterLogout(req, res, next){
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var state = false;

    User.update(
        {_id: currentUser._id},
        {$set:
        { state: state }},
        {upsert:false, $$runValidators:true},
        function(err) {
            if(err) {
                var errMsg = "";
                for( var key in err.errors) {
                    if(err.errors.hasOwnProperty(key)) {
                        errMsg = errMsg + err.errors[key].message;
                    }
                }
                return next({
                    status:400,
                    reason:errMsg
                });
            }
            return res.success();
        }
    );
}

async function getProfile(req, res, next){
    User.findOne({_id: req.decoded._id},
        function (err, doc) {
            if(err)
                return next(err);
            if(doc != null) {
                var profile = JSON.parse(JSON.stringify(doc));
                delete profile.__v;
                delete profile._id;

                res.send({
                    status:200,
                    me: profile
                });
            }
            else {
                res.send({
                    status: 404,
                    reason: "No user found"
                });
            }
        }
    )
}

async function updateProfile(req, res, next){
    var newProfile = req.body;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));

    User.update(
        {_id: currentUser._id},
        {$set: newProfile},
        {upsert:false, $$runValidators:true},
        function(err) {
            if(err) {
                var errMsg = "";
                for( var key in err.errors) {
                    if(err.errors.hasOwnProperty(key)) {
                        errMsg = errMsg + err.errors[key].message;
                    }
                }
                return next({
                    status:400,
                    reason:errMsg
                });
            }
            return res.success();
        }
    );
}

async function uploadAvatar(req, res, next) {
    if(!req.files.avatar) {
        return res.send({
            status: 400,
            reason: "Invaild avatar image"
        })
    }
    var avatar = req.files.avatar.name;
    var user_id = req.decoded._id;

    User.findOne({_id: req.decoded._id}, function (err, doc){
        if(err) return next(err);
        if(doc != null) {
            if(doc.avatar != ""){
                console.log(config.avatarUploadDir+doc.avatar);
                fs.unlinkSync(config.avatarUploadDir+doc.avatar);
            }
            User.update(
                {_id: user_id},
                { $set: {avatar:avatar} },
                {upsert:false, runValidators:true},
                function (err){
                    if(err) {
                        var errMsg = "";
                        for( var key in err.errors){
                            if(err.errors.hasOwnProperty(key)){
                                errMsg = errMsg + err.errors[key].message;
                            }
                        }
                        return next({
                            status:400,
                            reason:errMsg
                        });
                    }
                    res.send({
                        status:200,
                        avatar:config.avatarUploadDir+avatar
                    });
                }
            );
        }
        else{
            res.send({
                status: 404,
                reason: "No user found"
            });
        }
    });
}

async function updatePosition(req, res, next){
    var newLatitude = req.body.latitude;
    var newLongitude = req.body.longitude;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));

    if(newLongitude && newLatitude){
        var newPos = [newLongitude, newLatitude];
    }

    User.update(
        {_id: currentUser._id},
        {$set:
            { pos: newPos }},
        {upsert:false, $$runValidators:true},
        function(err) {
            if(err) {
                var errMsg = "";
                for( var key in err.errors) {
                    if(err.errors.hasOwnProperty(key)) {
                        errMsg = errMsg + err.errors[key].message;
                    }
                }
                return next({
                    status:400,
                    reason:errMsg
                });
            }
            return res.success();
        }
    );
}

async function updateChatId(req, res, next){
    var newChatId = req.body.chat_id;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));

    User.update(
        {_id: currentUser._id},
        {$set:
        { chat_id: newChatId }},
        {upsert:false, $$runValidators:true},
        function(err) {
            if(err) {
                var errMsg = "";
                for( var key in err.errors) {
                    if(err.errors.hasOwnProperty(key)) {
                        errMsg = errMsg + err.errors[key].message;
                    }
                }
                return next({
                    status:400,
                    reason:errMsg
                });
            }
            return res.success();
        }
    );
}


async function getFriendChatId(req, res, next){
    var friendId = req.query.friend_id;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var query = {};

    if(typeof friendId != 'undefined') {
        query.userID = friendId;
    }
    else{
        return res.send({
            status:400,
            reason: "Missing required fields"
        });
    }

    User.findOne(query)
        .exec(function (err, doc) {
            var profile = JSON.parse(JSON.stringify(doc));
            return res.send({
                status: 200,
                friend_chat_id: profile.chat_id
            });
        });
}

async function getMatchedFriends(req, res, next){
    var curLatitude = req.query.latitude;
    var curLongitude = req.query.longitude;
    var maxDistance = req.query.distance;
    var minAge = req.query.min_age;
    var maxAge = req.query.max_age;
    var gender = req.query.gender;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var query = {};

    if(typeof curLatitude != 'undefined' && typeof curLongitude != 'undefined' && typeof maxDistance != 'undefined'){
        var longitude = currentUser.longitude;
        var latitude = currentUser.latitude;
        query.pos = { $near : [ curLongitude, curLatitude ], $maxDistance : maxDistance };
    }
    else{
        return res.send({
            status:400,
            reason: "Missing required fields"
        });
    }

    query.age = { $gte: minAge, $lte: maxAge };

    if(gender != 2) { //male or female
        query.gender = gender;
    }

    var count = 0;
    var query1 = {like: { $in: [currentUser.userID] }};
    var friendIDs = [];

    Friend.count(query1, function(err, friendCount) {
        Friend.find(query1, function(err, friends) {
            for(var i = 0; i < friendCount; i++)
                friendIDs[i] = friends[i].userID;

            query.userID = {$in: friendIDs};
            User.find(query)
                .exec(function (err, doc) {
                    var profiles = JSON.parse(JSON.stringify(doc));
                    for(var i = 0; i < friendCount; i++) {
                        delete profiles[i].__v;
                        delete profiles[i]._id;
                        delete profiles[i].fb_id;
                    }
                    return res.send({
                        status: 200,
                        friends: profiles
                    });
                });
        });
    });
}

async function getRadarFriends(req, res, next){
    var curLatitude = req.query.latitude;
    var curLongitude = req.query.longitude;
    var maxDistance = req.query.distance;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var query = {};

    if(typeof curLatitude != 'undefined' && typeof curLongitude != 'undefined' && typeof maxDistance != 'undefined'){
        var longitude = req.query.longitude;
        var latitude = req.query.latitude;
        query.pos = { $near : [ longitude, latitude ], $maxDistance : maxDistance };
    }
    else{
        return res.send({
            status:400,
            reason: "Missing required fields"
        });
    }
    query.gender = !currentUser.gender;

    var count = 0;
    User.count(query, function( err, docCount) {
        count = docCount;
        User.find(query)
            .exec(function (err, events) {
                console.log(count);
                var profile = JSON.parse(JSON.stringify(events));
                delete profile.__v;
                delete profile._id;
                delete profile.fb_id;
                return res.send({
                    status: 200,
                    friends: events
                });
            });
    });
}

async function getFriendInfo(req, res, next){
    var friendId = req.query.friend_id;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var query = {};

    if(typeof friendId != 'undefined') {
        query.userID = friendId;
    }
    else{
        return res.send({
            status:400,
            reason: "Missing required fields"
        });
    }

    var count = 0;
    User.count(query, function( err, docCount) {
        count = docCount;
        User.findOne(query)
            .exec(function (err, doc) {
                var profile = JSON.parse(JSON.stringify(doc));
                delete profile.__v;
                delete profile._id;
                delete profile.fb_id;
                return res.send({
                    status: 200,
                    friend: profile
                });
            });
    });
}

async function updateRadarState(req, res, next){
    var radar_state = req.body.radar_state;
    var currentUser = JSON.parse(JSON.stringify(req.decoded));

    User.update(
        {_id: currentUser._id},
        {$set:
        { radar_state: radar_state }},
        {upsert:false, $$runValidators:true},
        function(err) {
            if(err) {
                var errMsg = "";
                for( var key in err.errors) {
                    if(err.errors.hasOwnProperty(key)) {
                        errMsg = errMsg + err.errors[key].message;
                    }
                }
                return next({
                    status:400,
                    reason:errMsg
                });
            }
            return res.success();
        }
    );
}

module.exports = {
    spotterLogin: spotterLogin,
    spotterLogout: spotterLogout,
    spotterDelete: spotterDelete,
    getProfile: getProfile,
    updateProfile: updateProfile,
    uploadAvatar: uploadAvatar,
    updatePosition: updatePosition,
    getMatchedFriends: getMatchedFriends,
    getRadarFriends: getRadarFriends,
    getFriendInfo: getFriendInfo,
    updateChatId: updateChatId,
    getFriendChatId: getFriendChatId,
    updateRadarState: updateRadarState
};
