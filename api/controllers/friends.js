/**
 * Created by dev on 16-1-12.
 */
let jwt    = require('jsonwebtoken');
let env = process.env.NODE_ENV || 'development';
let config = require('../../config/config')[env];

import { Friend } from '../models';
import { User } from '../models';

async function getLikeFriends(req, res, next) {
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    await Friend.findOne({userID: currentUser.userID}, function (err, result){
        if(err)
            return next(err);
        if(result != null) {
            var friends = result.like;
            User.find({userID: {$in: friends}}, function(err, friends_result){
                if(err){
                   if(err) {
                       return next(err);
                   }
                }
                if(friends_result){
                    res.send({
                        status: 200,
                        friends: friends_result
                    });
                } else {
                    res.send({
                        status: 404,
                        reason: "No Friends found"
                    });
                }
            });
        } else {
            res.send({
                status: 404,
                reason: "No Friends found"
            });
        }
    });
}

async function getDislikeFriends(req, res, next){
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    await Friend.findOne({userID: currentUser.userID}, function (err, result){
        if(err)
            return next(err);
        if(result != null) {
            var friends = result.dislike;
            User.find({userID: {$in: friends}}, function(err, friends_result){
                if(err){
                    if(err) {
                        return next(err);
                    }
                }
                if(friends_result){
                    res.send({
                        status: 200,
                        friends: friends_result
                    });
                } else {
                    res.send({
                        status: 404,
                        reason: "No Friends found"
                    });
                }
            });
        } else {
            res.send({
                status: 404,
                reason: "No Friends found"
            });
        }
    });
}

async function insertLikeFriend(req, res, next){
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var friend_id = req.body.friend_id;
    var user;

    await Friend.findOne({userID: currentUser.userID}, function (err, doc){
        if(err)
            return next(err);
        if(doc == null) {
            user = new Friend({userID: currentUser.userID});
            user.save(function (err) {
                if(err) {
                    var errMsg = "";
                    for( var key in err.errors) {
                        if(err.errors.hasOwnProperty(key)){
                            errMsg = errMsg + err.errors[key].message;
                        }
                    }
                    return res.error("error", 400, errMsg);
                }
            });
        }
    });

    var query1 = {userID: currentUser.userID};
    var query2 = {$push: {like: friend_id}};
    var query3 = {userID: currentUser.userID, like: {$in: [friend_id]}};
    Friend.find(query3, function(err, result) {
        if(err)
            return next(err);
        if (result == '') {
            Friend.update(query1, query2)
                .exec(function (err, doc) {
                    if (err) {
                        return next(err);
                    }
                    return res.success();
                });
        } else {
            return res.success();
        }
    });
}

async function insertDislikeFriend(req, res, next){
    var currentUser = JSON.parse(JSON.stringify(req.decoded));
    var friend_id = req.body.friend_id;
    var user;

    await Friend.findOne({userID: currentUser.userID}, function (err, doc){
        if(err)
            return next(err);
        if(doc == null) {
            user = new Friend({userID: currentUser.userID});
            user.save(function (err) {
                if(err) {
                    var errMsg = "";
                    for( var key in err.errors) {
                        if(err.errors.hasOwnProperty(key)){
                            errMsg = errMsg + err.errors[key].message;
                        }
                    }
                    return res.error("error", 400, errMsg);
                }
            });
        }
    });

    var query1 = {userID: currentUser.userID};
    var query2 = {$push: {dislike: friend_id}};
    var query3 = {userID: currentUser.userID, dislike: {$in: [friend_id]}};
    Friend.find(query3, function(err, result) {
        if(err)
            return next(err);
        if (result == '') {
            Friend.update(query1, query2)
                .exec(function (err, doc) {
                    if (err) {
                        return next(err);
                    }
                    return res.success();
                });
        } else {
            return res.success();
        }
    });
}

module.exports = {
    getLikeFriends: getLikeFriends,
    getDislikeFriends: getDislikeFriends,
    insertLikeFriend: insertLikeFriend,
    insertDislikeFriend: insertDislikeFriend
};
