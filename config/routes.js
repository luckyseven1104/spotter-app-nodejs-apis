import tokenCtrl from '../api/controllers/token';
import usersCtrl from '../api/controllers/users';
import friendCtrl from '../api/controllers/friends';
import successrateCtrl from '../api/controllers/successrate';

import resError from '../api/middlewares/res_error';
import resSuccess from '../api/middlewares/res_success';
import modelMagic from '../api/middlewares/model_magic';
var multer = require('multer');

let env = process.env.NODE_ENV || 'development';

var avatarUpload = multer({
    dest: './uploads/avatar',
    rename: function (fieldname, filename) {
        return filename + Date.now();
    }
});

module.exports = function ( app, passport ){

    app.use(resError);
    app.use(resSuccess);

    app.post('/api/auth/login', usersCtrl.spotterLogin);
    app.post('/api/auth/logout', tokenCtrl.ensureAuthenticated, usersCtrl.spotterLogout);
    app.delete('/api/auth/delete', tokenCtrl.ensureAuthenticated, usersCtrl.spotterDelete);

    app.get('/api/me', tokenCtrl.ensureAuthenticated, usersCtrl.getProfile);
    app.put('/api/me', tokenCtrl.ensureAuthenticated, usersCtrl.updateProfile);

    app.put('/api/me/chatid', tokenCtrl.ensureAuthenticated, usersCtrl.updateChatId);
    app.get('/api/friend/chatid', tokenCtrl.ensureAuthenticated, usersCtrl.getFriendChatId);

    app.put('/api/me/radar', tokenCtrl.ensureAuthenticated, usersCtrl.updateRadarState);

    app.post('/api/me/avatar', tokenCtrl.ensureAuthenticated, avatarUpload, usersCtrl.uploadAvatar);
    app.put('/api/me/position', tokenCtrl.ensureAuthenticated, usersCtrl.updatePosition);

    app.get('/api/friend/profile', tokenCtrl.ensureAuthenticated, usersCtrl.getFriendInfo);
    app.get('/api/friend/match', tokenCtrl.ensureAuthenticated, usersCtrl.getMatchedFriends);
    app.get('/api/friend/radar', tokenCtrl.ensureAuthenticated, usersCtrl.getRadarFriends);

    app.get('/api/friend/like', tokenCtrl.ensureAuthenticated, friendCtrl.getLikeFriends);
    app.get('/api/friend/dislike', tokenCtrl.ensureAuthenticated, friendCtrl.getDislikeFriends);
    app.put('/api/friend/like', tokenCtrl.ensureAuthenticated, friendCtrl.insertLikeFriend);
    app.put('/api/friend/dislike', tokenCtrl.ensureAuthenticated, friendCtrl.insertDislikeFriend);
};