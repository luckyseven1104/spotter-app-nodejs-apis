﻿
import path from 'path';
let rootPath = path.normalize(__dirname + '/..');

module.exports = {
	development: {
		db: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/spotter_dev',
		root: rootPath,
		app: {
			name: 'Spotter Backend API'
		},
		secret:'spotter secret',
		avatarUploadDir: 'uploads/avatar/',
		avatarUploadUrl: 'https://54.218.54.176/spotter/uploads/avatar/'
	},
	test: {
		db: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/spotter_test',
		root: rootPath,
		app: {
			name: 'Spotter Backend API'
		},
		secret:'spotter secret'
	},
	production: {
		db: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/spotter_prod',
		root: rootPath,
		app: {
			name: 'Spotter Backend API'
		},
		secret:'spotter secret'
	}
};
