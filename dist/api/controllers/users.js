'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _models = require('../models');

var jwt = require('jsonwebtoken');
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config')[env];

var fs = require('fs');

function spotterLogin(req, res, next) {
    var fb_id, user, token;
    return _regeneratorRuntime.async(function spotterLogin$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                fb_id = req.body.fb_id;

                if (!(!fb_id || fb_id == "")) {
                    context$1$0.next = 3;
                    break;
                }

                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Facebook id is not valid."
                }));

            case 3:
                context$1$0.next = 5;
                return _regeneratorRuntime.awrap(_models.User.findOneAsync({ fb_id: fb_id }));

            case 5:
                user = context$1$0.sent;

                if (!user) {
                    signup(req, res, next);
                }

                user.state = true;
                user.save(function (err) {
                    if (err) {
                        return res.error("register_error", 400);
                    }
                });

                token = jwt.sign(user, config.secret, {
                    expiresInMinutes: 1440 // expires in 24 hours
                });

                res.success({ id: user._id, api_token: token });

            case 11:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function signup(req, res, next) {
    var user;
    return _regeneratorRuntime.async(function signup$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                user = new _models.User(req.body);

                user.userID = user._id;
                user.state = true;

                user.save(function (err) {
                    if (err) {
                        var errMsg = "";
                        for (var key in err.errors) {
                            if (err.errors.hasOwnProperty(key)) {
                                errMsg = errMsg + err.errors[key].message;
                            }
                        }

                        return res.error("register_error", 400, errMsg);
                    }
                    _models.User.findOne({ fb_id: user.fb_id }, function (err, doc) {
                        if (err) {
                            return done(err);
                        }

                        var token = jwt.sign(doc, config.secret, {
                            expiresInMinutes: 1440 // expires in 24 hours
                        });

                        var response = {};
                        response.id = doc._id;
                        response.api_token = token;
                        return res.success(response);
                    });
                });

            case 4:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function spotterDelete(req, res, next) {
    return _regeneratorRuntime.async(function spotterDelete$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                console.log('user delete');
                context$1$0.next = 3;
                return _regeneratorRuntime.awrap(req.user.removeAsync());

            case 3:
                return context$1$0.abrupt('return', res.success());

            case 4:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function spotterLogout(req, res, next) {
    var currentUser, state;
    return _regeneratorRuntime.async(function spotterLogout$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                currentUser = JSON.parse(JSON.stringify(req.decoded));
                state = false;

                _models.User.update({ _id: currentUser._id }, { $set: { state: state } }, { upsert: false, $$runValidators: true }, function (err) {
                    if (err) {
                        var errMsg = "";
                        for (var key in err.errors) {
                            if (err.errors.hasOwnProperty(key)) {
                                errMsg = errMsg + err.errors[key].message;
                            }
                        }
                        return next({
                            status: 400,
                            reason: errMsg
                        });
                    }
                    return res.success();
                });

            case 3:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function getProfile(req, res, next) {
    return _regeneratorRuntime.async(function getProfile$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                _models.User.findOne({ _id: req.decoded._id }, function (err, doc) {
                    if (err) return next(err);
                    if (doc != null) {
                        var profile = JSON.parse(JSON.stringify(doc));
                        delete profile.__v;
                        delete profile._id;

                        res.send({
                            status: 200,
                            me: profile
                        });
                    } else {
                        res.send({
                            status: 404,
                            reason: "No user found"
                        });
                    }
                });

            case 1:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function updateProfile(req, res, next) {
    var newProfile, currentUser;
    return _regeneratorRuntime.async(function updateProfile$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                newProfile = req.body;
                currentUser = JSON.parse(JSON.stringify(req.decoded));

                _models.User.update({ _id: currentUser._id }, { $set: newProfile }, { upsert: false, $$runValidators: true }, function (err) {
                    if (err) {
                        var errMsg = "";
                        for (var key in err.errors) {
                            if (err.errors.hasOwnProperty(key)) {
                                errMsg = errMsg + err.errors[key].message;
                            }
                        }
                        return next({
                            status: 400,
                            reason: errMsg
                        });
                    }
                    return res.success();
                });

            case 3:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function uploadAvatar(req, res, next) {
    var avatar, user_id;
    return _regeneratorRuntime.async(function uploadAvatar$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                if (req.files.avatar) {
                    context$1$0.next = 2;
                    break;
                }

                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Invaild avatar image"
                }));

            case 2:
                avatar = req.files.avatar.name;
                user_id = req.decoded._id;

                _models.User.findOne({ _id: req.decoded._id }, function (err, doc) {
                    if (err) return next(err);
                    if (doc != null) {
                        if (doc.avatar != "") {
                            console.log("uploads/avatar/" + doc.avatar);
                            fs.unlinkSync("uploads/avatar/" + doc.avatar);
                        }
                        _models.User.update({ _id: user_id }, { $set: { avatar: avatar } }, { upsert: false, runValidators: true }, function (err) {
                            if (err) {
                                var errMsg = "";
                                for (var key in err.errors) {
                                    if (err.errors.hasOwnProperty(key)) {
                                        errMsg = errMsg + err.errors[key].message;
                                    }
                                }
                                return next({
                                    status: 400,
                                    reason: errMsg
                                });
                            }
                            res.send({
                                status: 200
                            });
                        });
                    } else {
                        res.send({
                            status: 404,
                            reason: "No user found"
                        });
                    }
                });

            case 5:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function updatePosition(req, res, next) {
    var newLatitude, newLongitude, currentUser, newPos;
    return _regeneratorRuntime.async(function updatePosition$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                newLatitude = req.body.latitude;
                newLongitude = req.body.longitude;
                currentUser = JSON.parse(JSON.stringify(req.decoded));

                if (newLongitude && newLatitude) {
                    newPos = [newLongitude, newLatitude];
                }

                _models.User.update({ _id: currentUser._id }, { $set: { pos: newPos } }, { upsert: false, $$runValidators: true }, function (err) {
                    if (err) {
                        var errMsg = "";
                        for (var key in err.errors) {
                            if (err.errors.hasOwnProperty(key)) {
                                errMsg = errMsg + err.errors[key].message;
                            }
                        }
                        return next({
                            status: 400,
                            reason: errMsg
                        });
                    }
                    return res.success();
                });

            case 5:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function getMatchedFriends(req, res, next) {
    var curLatitude, curLongitude, maxDistance, minAge, maxAge, gender, currentUser, query, longitude, latitude, count;
    return _regeneratorRuntime.async(function getMatchedFriends$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                curLatitude = req.query.latitude;
                curLongitude = req.query.longitude;
                maxDistance = req.query.distance;
                minAge = req.query.min_age;
                maxAge = req.query.max_age;
                gender = req.query.gender;
                currentUser = JSON.parse(JSON.stringify(req.decoded));
                query = {};

                if (!(typeof curLatitude != 'undefined' && typeof curLongitude != 'undefined' && typeof maxDistance != 'undefined')) {
                    context$1$0.next = 14;
                    break;
                }

                longitude = req.query.longitude;
                latitude = req.query.latitude;

                query.pos = { $near: [longitude, latitude], $maxDistance: maxDistance };
                context$1$0.next = 15;
                break;

            case 14:
                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Missing required fields"
                }));

            case 15:

                query.age = { $gte: minAge, $lte: maxAge };

                if (gender != 2) {
                    //male or female
                    query.gender = gender;
                }

                count = 0;

                _models.User.count(query, function (err, docCount) {
                    count = docCount;
                    _models.User.find(query).exec(function (err, doc) {
                        console.log(count);
                        var profile = JSON.parse(JSON.stringify(doc));
                        profile.userID = profile._id;
                        delete profile.__v;
                        delete profile._id;
                        delete profile.fb_id;
                        return res.send({
                            status: 200,
                            friends: events
                        });
                    });
                });

            case 19:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function getRadarFriends(req, res, next) {
    var curLatitude, curLongitude, maxDistance, currentUser, query, longitude, latitude, count;
    return _regeneratorRuntime.async(function getRadarFriends$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                curLatitude = req.query.latitude;
                curLongitude = req.query.longitude;
                maxDistance = req.query.distance;
                currentUser = JSON.parse(JSON.stringify(req.decoded));
                query = {};

                if (!(typeof curLatitude != 'undefined' && typeof curLongitude != 'undefined' && typeof maxDistance != 'undefined')) {
                    context$1$0.next = 11;
                    break;
                }

                longitude = req.query.longitude;
                latitude = req.query.latitude;

                query.pos = { $near: [longitude, latitude], $maxDistance: maxDistance };
                context$1$0.next = 12;
                break;

            case 11:
                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Missing required fields"
                }));

            case 12:
                query.gender = !currentUser.gender;

                count = 0;

                _models.User.count(query, function (err, docCount) {
                    count = docCount;
                    _models.User.find(query).exec(function (err, events) {
                        console.log(count);
                        var profile = JSON.parse(JSON.stringify(doc));
                        delete profile.__v;
                        delete profile._id;
                        delete profile.fb_id;
                        return res.send({
                            status: 200,
                            friends: events
                        });
                    });
                });

            case 15:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function getFriendInfo(req, res, next) {
    var friendId, currentUser, query, count;
    return _regeneratorRuntime.async(function getFriendInfo$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                friendId = req.query.friend_id;
                currentUser = JSON.parse(JSON.stringify(req.decoded));
                query = {};

                if (!(typeof friendId != 'undefined')) {
                    context$1$0.next = 7;
                    break;
                }

                query.userID = friendId;
                context$1$0.next = 8;
                break;

            case 7:
                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Missing required fields"
                }));

            case 8:
                count = 0;

                _models.User.count(query, function (err, docCount) {
                    count = docCount;
                    _models.User.find(query).exec(function (err, doc) {
                        console.log(count);
                        var profile = JSON.parse(JSON.stringify(doc));
                        delete profile.__v;
                        delete profile._id;
                        delete profile.fb_id;
                        return res.send({
                            status: 200,
                            friend: events
                        });
                    });
                });

            case 10:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

module.exports = {
    spotterLogin: spotterLogin,
    spotterLogout: spotterLogout,
    spotterDelete: spotterDelete,
    getProfile: getProfile,
    updateProfile: updateProfile,
    uploadAvatar: uploadAvatar,
    updatePosition: updatePosition,
    getMatchedFriends: getMatchedFriends,
    getRadarFriends: getRadarFriends,
    getFriendInfo: getFriendInfo
};