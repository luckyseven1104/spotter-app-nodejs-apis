/**
 * Created by dev on 16-1-12.
 */
'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _models = require('../models');

var jwt = require('jsonwebtoken');
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config')[env];

function getLikeFriends(req, res, next) {
    var query, count;
    return _regeneratorRuntime.async(function getLikeFriends$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                query = { userID: req.decoded._id };
                count = 0;

                _models.Friend.count(query, function (err, docCount) {
                    count = docCount;
                    _models.Friend.find(query).exec(function (err, events) {
                        console.log(count);
                        if (err) {
                            return next(err);
                        }
                        if (events != null) {
                            var friends = JSON.parse(JSON.stringify(events));
                            delete friends.dislike;

                            return res.send({
                                status: 200,
                                friends: events
                            });
                        }
                    });
                });

            case 3:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function getDislikeFriends(req, res, next) {
    var query, count;
    return _regeneratorRuntime.async(function getDislikeFriends$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                query = { userID: req.decoded._id };
                count = 0;

                _models.Friend.count(query, function (err, docCount) {
                    count = docCount;
                    _models.Friend.find(query).exec(function (err, events) {
                        console.log(count);
                        if (err) {
                            return next(err);
                        }
                        if (events != null) {
                            var friends = JSON.parse(JSON.stringify(events));
                            delete friends.like;

                            return res.send({
                                status: 200,
                                friends: events
                            });
                        }
                    });
                });

            case 3:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function insertLikeFriend(req, res, next) {
    var friend_id, query;
    return _regeneratorRuntime.async(function insertLikeFriend$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                friend_id = req.body.friend_id;
                query = { userID: req.decoded._id };

                query = { $push: { like: friend_id } };

                if (!(typeof friend_id != 'undefined')) {
                    context$1$0.next = 5;
                    break;
                }

                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Missing required fields"
                }));

            case 5:

                _models.Friend.update(query).exec(function (err, doc) {
                    if (err) {
                        return next(err);
                    }
                    return res.success();
                });

            case 6:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function insertDislikeFriend(req, res, next) {
    var friend_id, query;
    return _regeneratorRuntime.async(function insertDislikeFriend$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                friend_id = req.body.friend_id;
                query = { userID: req.decoded._id };

                query = { $push: { dislike: friend_id } };

                if (!(typeof friend_id != 'undefined')) {
                    context$1$0.next = 5;
                    break;
                }

                return context$1$0.abrupt('return', res.send({
                    status: 400,
                    reason: "Missing required fields"
                }));

            case 5:

                _models.Friend.update(query).exec(function (err, doc) {
                    if (err) {
                        return next(err);
                    }
                    return res.success();
                });

            case 6:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

module.exports = {
    getLikeFriends: getLikeFriends,
    getDislikeFriends: getDislikeFriends,
    insertLikeFriend: insertLikeFriend,
    insertDislikeFriend: insertDislikeFriend
};