//var mongoose = require ('mongoose'),
//    User = mongoose.model('User')
'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _models = require('../models');

var jwt = require('jsonwebtoken');
var env = process.env.NODE_ENV || 'development';
var config = require('../../config/config')[env];

function save(req, res, next) {
    var _req$body, input1, input2, success, user, myinputs;

    return _regeneratorRuntime.async(function save$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                _req$body = req.body;
                input1 = _req$body.input1;
                input2 = _req$body.input2;
                success = _req$body.success;
                user = req.user._id;
                myinputs = { input1: input1, input2: input2, user: user };
                context$1$0.next = 8;
                return _regeneratorRuntime.awrap(_models.SuccessRate.saveResult(myinputs, success));

            case 8:
                res.success({});

            case 9:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function predictInputOne(req, res, next) {
    var input1, response;
    return _regeneratorRuntime.async(function predictInputOne$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                input1 = req.params.input1;
                context$1$0.next = 3;
                return _regeneratorRuntime.awrap(_models.SuccessRate.getInput1Prediction(input1));

            case 3:
                response = context$1$0.sent;

                res.success({ predictions: response });

            case 5:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

function index(req, res, next) {
    var user, response;
    return _regeneratorRuntime.async(function index$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                user = req.user._id;
                context$1$0.next = 3;
                return _regeneratorRuntime.awrap(_models.SuccessRate.findAsync({ user: user }));

            case 3:
                response = context$1$0.sent;

                res.success({ data: response });

            case 5:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
}

module.exports = {
    save: save,
    index: index,
    predictInputOne: predictInputOne
};