/**
 * Module dependencies.
 */

'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

var _mongooseModelUpdate = require('mongoose-model-update');

var _mongooseModelUpdate2 = _interopRequireDefault(_mongooseModelUpdate);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

/**
 * User Schema
 */

var crypto = require('crypto');
var User = new _mongoose.Schema({
    fb_id: {
        type: String,
        unique: true,
        required: 'Facebook ID is required. '
    },
    userID: { type: String, required: true },
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    avatar: { type: String, 'default': '' },
    lifeStyle: {
        WeightTraining: { type: Boolean, 'default': false },
        OutdoorSports: { type: Boolean, 'default': false },
        Yoga: { type: Boolean, 'default': false },
        TeamSports: { type: Boolean, 'default': false },
        Cycling: { type: Boolean, 'default': false },
        Hiking: { type: Boolean, 'default': false },
        MMABoxing: { type: Boolean, 'default': false },
        Running: { type: Boolean, 'default': false }
    },
    height: { type: Number },
    education: { type: String },
    career: { type: String },
    style: { type: String },
    ethnicity: { type: String },
    age: { type: Number },
    gender: { type: Number },
    pos: {
        type: [Number], //latitude, longitude
        index: '2d'
    },
    about: { type: String },
    state: { type: Boolean, 'default': false } //true:login, false:logout
});

/**
 * Methods
 */

User.methods = {

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */

    authenticate: function authenticate(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */

    updatePassword: function updatePassword(password) {
        return _regeneratorRuntime.async(function updatePassword$(context$1$0) {
            while (1) switch (context$1$0.prev = context$1$0.next) {
                case 0:
                    this._password = password;
                    this.salt = this.makeSalt();
                    this.hashed_password = this.encryptPassword(password);
                    console.log(this);
                    context$1$0.next = 6;
                    return _regeneratorRuntime.awrap(this.saveAsync());

                case 6:
                    return context$1$0.abrupt('return', context$1$0.sent[0]);

                case 7:
                case 'end':
                    return context$1$0.stop();
            }
        }, null, this);
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */

    makeSalt: function makeSalt() {
        return Math.round(new Date().valueOf() * Math.random()) + '';
    },

    addAvatar: function addAvatar(avatar) {
        this.image = avatar;
    },

    getAvatar: function getAvatar() {
        return this.image;
    }
};

module.exports = _mongoose2['default'].model('User', User);
_bluebird2['default'].promisifyAll(module.exports);
_bluebird2['default'].promisifyAll(module.exports.prototype);