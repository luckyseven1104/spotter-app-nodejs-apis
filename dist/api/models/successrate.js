'use strict';

var _regeneratorRuntime = require('babel-runtime/regenerator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _mongooseTimestamp = require('mongoose-timestamp');

var _mongooseTimestamp2 = _interopRequireDefault(_mongooseTimestamp);

var _mongooseModelUpdate = require('mongoose-model-update');

var _mongooseModelUpdate2 = _interopRequireDefault(_mongooseModelUpdate);

/**
 * File Schema
 */

var SuccessRate = new _mongoose.Schema({
    user: {
        type: _mongoose.Schema.ObjectId,
        ref: 'User',
        required: 'Event host cannot be blank. '
    },
    input1: {
        type: String,
        required: true
    },
    input2: {
        type: String,
        required: true
    },
    success: {
        type: Number,
        'default': 0
    },
    count: {
        type: Number,
        'default': 1
    }
});

SuccessRate.plugin(_mongooseTimestamp2['default']);
//File.plugin(update, ['user', 'version_id', 'file']);

SuccessRate.statics.saveResult = function callee$0$0(myinputs, success) {
    var existing;
    return _regeneratorRuntime.async(function callee$0$0$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                context$1$0.next = 2;
                return _regeneratorRuntime.awrap(this.findOneAsync(myinputs));

            case 2:
                existing = context$1$0.sent;

                console.log(existing);

                if (existing) {
                    context$1$0.next = 11;
                    break;
                }

                if (success) myinputs.success = 1;
                context$1$0.next = 8;
                return _regeneratorRuntime.awrap(new this(myinputs).saveAsync());

            case 8:
                return context$1$0.abrupt('return', true);

            case 11:
                existing.count++;
                if (success) existing.success++;
                context$1$0.next = 15;
                return _regeneratorRuntime.awrap(existing.saveAsync());

            case 15:
                return context$1$0.abrupt('return', true);

            case 16:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
};

SuccessRate.statics.getInput1Prediction = function callee$0$0(input1) {
    var predictions;
    return _regeneratorRuntime.async(function callee$0$0$(context$1$0) {
        while (1) switch (context$1$0.prev = context$1$0.next) {
            case 0:
                context$1$0.next = 2;
                return _regeneratorRuntime.awrap(this.findAsync({ "input1": { "$regex": input1, "$options": "i" } }));

            case 2:
                predictions = context$1$0.sent;
                return context$1$0.abrupt('return', predictions);

            case 4:
            case 'end':
                return context$1$0.stop();
        }
    }, null, this);
};

module.exports = _mongoose2['default'].model('SuccessRate', SuccessRate);
_bluebird2['default'].promisifyAll(module.exports);
_bluebird2['default'].promisifyAll(module.exports.prototype);