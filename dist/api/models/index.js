'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _configDatabase = require('../../config/database');

var _configDatabase2 = _interopRequireDefault(_configDatabase);

module.exports = {
    User: require('./user'),
    Friend: require('./friend'),
    SuccessRate: require('./successrate')
};