'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var rootPath = _path2['default'].normalize(__dirname + '/..');

module.exports = {
	development: {
		db: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/spotter_dev',
		root: rootPath,
		app: {
			name: 'Spotter Backend API'
		},
		secret: 'spotter secret'
	},
	test: {
		db: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/spotter_test',
		root: rootPath,
		app: {
			name: 'Spotter Backend API'
		},
		secret: 'spotter secret'
	},
	production: {
		db: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/spotter_prod',
		root: rootPath,
		app: {
			name: 'Spotter Backend API'
		},
		secret: 'spotter secret'
	}
};