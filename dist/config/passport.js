'use strict';

var _apiModels = require('../api/models');

var LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport) {
    // require('./initializer')

    // serialize sessions
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        _apiModels.User.findOne({ _id: id }, function (err, user) {
            done(err, user);
        });
    });
};