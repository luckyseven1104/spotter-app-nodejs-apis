'use strict';

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

var _apiControllersToken = require('../api/controllers/token');

var _apiControllersToken2 = _interopRequireDefault(_apiControllersToken);

var _apiControllersUsers = require('../api/controllers/users');

var _apiControllersUsers2 = _interopRequireDefault(_apiControllersUsers);

var _apiControllersFriends = require('../api/controllers/friends');

var _apiControllersFriends2 = _interopRequireDefault(_apiControllersFriends);

var _apiControllersSuccessrate = require('../api/controllers/successrate');

var _apiControllersSuccessrate2 = _interopRequireDefault(_apiControllersSuccessrate);

var _apiMiddlewaresRes_error = require('../api/middlewares/res_error');

var _apiMiddlewaresRes_error2 = _interopRequireDefault(_apiMiddlewaresRes_error);

var _apiMiddlewaresRes_success = require('../api/middlewares/res_success');

var _apiMiddlewaresRes_success2 = _interopRequireDefault(_apiMiddlewaresRes_success);

var _apiMiddlewaresModel_magic = require('../api/middlewares/model_magic');

var _apiMiddlewaresModel_magic2 = _interopRequireDefault(_apiMiddlewaresModel_magic);

var multer = require('multer');

var env = process.env.NODE_ENV || 'development';

var avatarUpload = multer({
    dest: './uploads/avatar',
    rename: function rename(fieldname, filename) {
        return filename + Date.now();
    }
});

module.exports = function (app, passport) {

    app.use(_apiMiddlewaresRes_error2['default']);
    app.use(_apiMiddlewaresRes_success2['default']);

    app.post('/api/auth/login', _apiControllersUsers2['default'].spotterLogin);
    app.post('/api/auth/logout', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].spotterLogout);
    app['delete']('/api/auth/delete', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].spotterDelete);

    app.get('/api/me', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].getProfile);
    app.put('/api/me', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].updateProfile);

    app.post('/api/me/avatar', _apiControllersToken2['default'].ensureAuthenticated, avatarUpload, _apiControllersUsers2['default'].uploadAvatar);
    app.put('/api/me/position', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].updatePosition);

    app.get('/api/friend/profile', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].getFriendInfo);
    app.get('/api/friend/match', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].getMatchedFriends);
    app.get('/api/friend/radar', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersUsers2['default'].getRadarFriends);

    app.get('/api/friend/like', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersFriends2['default'].getLikeFriends);
    app.get('/api/friend/dislike', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersFriends2['default'].getDislikeFriends);
    app.put('/api/friend/like', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersFriends2['default'].insertLikeFriend);
    app.put('/api/friend/dislike', _apiControllersToken2['default'].ensureAuthenticated, _apiControllersFriends2['default'].insertDislikeFriend);
};